# provides interface for interacting with tabular data
import pandas as pd
# combines the capabilities of pandas and shapely for geospatial operations
import geopandas as gpd
# for manipulating text data into geospatial shapes
from shapely.geometry import Point

# Read in PUDs file as a geodataframe and initialize coordinate reference system (CRS)
puds = gpd.read_file('./data/Planned_Unit_Development_PUDs.shp', crs={'init': 'epsg:4326'})
aff = pd.read_csv('./data/Affordable_Housing.csv')
crosswalk = pd.read_csv('./data/zoning_crosswalk.csv')

# Create a geometry column in the affordable housing dataframe
aff['geometry'] = aff.apply(lambda row: Point(row.X, row.Y), axis=1)
aff = gpd.GeoDataFrame(aff, crs={'init': 'epsg:4326'})

# Use geospatial join to identify which PUDs include affordable housing projects
puds_aff = gpd.sjoin(puds, aff, op='intersects', how='left')

# Merge dataframe with zoning categories crosswalk
puds_info = puds_aff.merge(crosswalk[['Zone_Cat']], how='left', left_on='PUD_ZONING', right_on=crosswalk['Zone'])

# Create a map of PUDs by Zoning Category
puds_info.plot(column='Zone_Cat', legend=True, figsize=(16, 8))

# Export geodataframe as geojson
puds_info.to_file('./results/puds_info.geojson', driver='GeoJSON')
