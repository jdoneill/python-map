# import pandas as pd
import geopandas as gpd

# read shp
trails = gpd.read_file('./data/seattle-trails.shp', crs={'init': 'epsg:4326'})
parks = gpd.read_file('./data/seattle-parks.shp', crs={'init': 'epsg:4326'})

# convert to geojson
trails.to_file('./results/seattle-trails.geojson', driver='GeoJSON')
parks.to_file('./results/seattle-parks.geojson', driver='GeoJSON')
