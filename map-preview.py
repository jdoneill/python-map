import geopandas as gpd
import webbrowser

f = open('map.html', 'w')

g = gpd.read_file('results/puds_info.geojson')
print(g.head())
geojson = g.to_json()

messageBegin = """
<html>
<head>
  <title>A Leaflet map!</title>
   <link rel="stylesheet" href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css"
    integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ=="
    crossorigin=""/>
  <script src="https://unpkg.com/leaflet@1.6.0/dist/leaflet.js"
    integrity="sha512-gZwIG9x3wUXg2hdXF6+rVkLF/0Vi9U8D2Ntg4Ga5I5BZpVkVxlJWbSQtXPSiUTtC0TjtGOmxa1AJPuV0CPthew=="
    crossorigin=""></script>
  <style>
    #map{ height: 100% }
  </style>
</head>
<body>
  <div id="map"></div>
  <script>
    mapLink = '<a href="http://openstreetmap.org">OpenStreetMap</a>';
    var tiles = L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
      attribution: '&copy; ' + mapLink + ' Contributors'
    });
 """

geojsonFeature = "    var geojsonFeature = {};".format(geojson)

messageEnd = """
    var map = L.map('map');
    tiles.addTo(map);
    L.geoJSON(geojsonFeature).addTo(map);
    map.fitBounds(L.geoJSON(geojsonFeature).getBounds())
  </script>
</body>
</head>
"""

html = messageBegin + geojsonFeature + messageEnd

f.write(html)
f.close

filename = 'file:///Users/dop/dev/gitlab-jdoneill/python-map/' + 'map.html'
webbrowser.open_new_tab(filename)
